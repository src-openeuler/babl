Name:           babl
Version:        0.1.112
Release:        1
Summary:        Dynamic Pixel Format Translation Library
License:        LGPL-3.0-or-later AND GPL-3.0-or-later
Group:          Development/Libraries/C and C++
URL:            https://gegl.org/babl/

Source0:        https://download.gimp.org/pub/babl/0.1/%{name}-%{version}.tar.xz

BuildRequires:  meson >= 0.55.0
BuildRequires:  vala
BuildRequires:  pkgconfig(gobject-introspection-1.0) >= 1.32.0
BuildRequires:  pkgconfig(lcms2) >= 2.8
BuildRequires:  pkgconfig(vapigen) >= 0.20.0

%description
babl is a dynamic, any to any, pixel format translation library.

%package devel
Summary:        Dynamic Pixel Format Translation Library
Group:          Development/Libraries/C and C++
Requires:       %{name} = %{version}-%{release}

%description devel
babl is a dynamic, any to any, pixel format translation library.

%package vala
Summary:        Vala Bindings to the Dynamic Pixel Format Translation Library
Group:          Development/Libraries/C and C++
Requires:       %{name} = %{version}
Requires:       vala

%description vala
babl is a dynamic, any to any, pixel format translation library.

This package provides Vala bindings for babl.

%prep
%autosetup -p1

%build
%meson -Dwith-docs=false -Dgi-docgen=disabled \
	%{nil}
%meson_build

%install
%meson_install

%check
%meson_test

%files
%license COPYING
%doc NEWS
%{_bindir}/%{name}
%{_libdir}/*.so.*
%{_libdir}/babl-0.1/
%{_libdir}/girepository-1.0/Babl-0.1.typelib

%files devel
%doc AUTHORS TODO
%{_includedir}/babl-0.1/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/Babl-0.1.gir

%files vala
%{_datadir}/vala/vapi/babl-0.1.deps
%{_datadir}/vala/vapi/babl-0.1.vapi

%changelog
* Mon Mar 17 2025 Funda Wang <fundawang@yeah.net> - 0.1.112-1
- update to 0.1.112

* Mon Oct 28 2024 Funda Wang <fundawang@yeah.net> - 0.1.110-1
- update to 0.1.110

* Wed Oct 18 2023 xu_ping <707078654@qq.com> - 0.1.106-1
- Upgrade version to 0.1.106

* Wed Jan 18 2023 yaoxin <yaoxin30@h-partners.com> - 0.1.88-2
- Fix build error

* Tue Aug 24 2021 chenchen <chen_aka_jan@163.com> - 0.1.88-1
- update to 0.1.88

* Mon May 31 2021 baizhonggui <baizhonggui@huawei.com> - 0.1.56-4
- Add gcc in BuildRequires

* Fri Dec 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.1.56-3
- Package init
